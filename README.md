# Examen Lasec

### Prerrequisitos

net core 2.1

Crear Base de Datos en sqlServer
```SQL
USE [master]
CREATE DATABASE [EXAMEN_LASEC];
```


migraciones de la base de datos
```Sh
dotnet ef migrations remove

dotnet restore 

dotnet ef migrations add InitialCreate --context LasecExamenContext

dotnet ef database update --context LasecExamenContext
```
### instalación

Cambio para que la fecha se ponga automatica
```SQL
ALTER TABLE [Log_Carrera] ADD CONSTRAINT fecha_carrera_ganada_c DEFAULT SYSDATETIMEOFFSET() FOR fecha_carrera_ganada;
```

## Ejecucion

```Sh
dotnet run
```

## Autor

* **Leon Quezada** - *ingeniero en software* - [GitLab](https://gitlab.com/LeonQuezada/)
