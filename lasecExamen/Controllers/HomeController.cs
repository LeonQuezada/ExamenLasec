using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using lasecExamen.Models;
 
namespace lasecExamen.Controllers{
	public class ItemViewModel{
            public List<Animal> Animales { get; set; }
            public List<Carrera> Carreras { get; set; }
		    public List<Log_Carrera> Log_Carreras { get; set; }
        } 
    public class HomeController : Controller{
		private readonly LasecExamenContext _context;

		public HomeController(LasecExamenContext context)
        {
            _context = context;
        }




        // GET: Animal
        public async Task<IActionResult> Index()
        {
			var animales = _context.Animal.ToListAsync();
			var carreras = _context.Carrera.ToListAsync();
			var Log_Carreras= _context.Log_Carrera.ToListAsync();

			await Task.WhenAll(animales, carreras);
			var vm = new ItemViewModel();
			vm.Animales = await animales;
			vm.Carreras = await carreras;
			vm.Log_Carreras = await Log_Carreras;
			return View(vm);
        }      
        public IActionResult About(){
            ViewData["Message"] = "Your application description page.";
 
            return View();
        }
 
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }
        public IActionResult Privacy()
        {
            return View(); 
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
 