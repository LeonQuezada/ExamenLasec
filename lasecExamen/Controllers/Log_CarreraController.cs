using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using lasecExamen.Models;

namespace lasecExamen.Controllers
{
    public class Log_CarreraController : Controller
    {
        private readonly LasecExamenContext _context;

        public Log_CarreraController(LasecExamenContext context)
        {
            _context = context;
        }

        // GET: Log_Carrera
        public async Task<IActionResult> Index()
        {
            return View(await _context.Log_Carrera.ToListAsync());
        }

        // GET: Log_Carrera/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var log_Carrera = await _context.Log_Carrera
                .FirstOrDefaultAsync(m => m.id_log_carrera == id);
            if (log_Carrera == null)
            {
                return NotFound();
            }

            return View(log_Carrera);
        }

        // GET: Log_Carrera/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Log_Carrera/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id_log_carrera,nombre_Animal,nombre_CarreraId,fecha_carrera_ganada")] Log_Carrera log_Carrera)
        {
            if (ModelState.IsValid)
            {
                _context.Add(log_Carrera);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(log_Carrera);
        }

        // GET: Log_Carrera/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var log_Carrera = await _context.Log_Carrera.FindAsync(id);
            if (log_Carrera == null)
            {
                return NotFound();
            }
            return View(log_Carrera);
        }

        // POST: Log_Carrera/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id_log_carrera,nombre_Animal,nombre_CarreraId,fecha_carrera_ganada")] Log_Carrera log_Carrera)
        {
            if (id != log_Carrera.id_log_carrera)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(log_Carrera);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Log_CarreraExists(log_Carrera.id_log_carrera))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(log_Carrera);
        }

        // GET: Log_Carrera/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var log_Carrera = await _context.Log_Carrera
                .FirstOrDefaultAsync(m => m.id_log_carrera == id);
            if (log_Carrera == null)
            {
                return NotFound();
            }

            return View(log_Carrera);
        }

        // POST: Log_Carrera/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var log_Carrera = await _context.Log_Carrera.FindAsync(id);
            _context.Log_Carrera.Remove(log_Carrera);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool Log_CarreraExists(int id)
        {
            return _context.Log_Carrera.Any(e => e.id_log_carrera == id);
        }
    }
}
