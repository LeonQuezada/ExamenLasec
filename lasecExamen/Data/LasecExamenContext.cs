using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using lasecExamen.Models;

    public class LasecExamenContext : DbContext
    {
        public LasecExamenContext (DbContextOptions<LasecExamenContext> options)
            : base(options)
        {
        }

        public DbSet<lasecExamen.Models.Animal> Animal { get; set; }

        public DbSet<lasecExamen.Models.Carrera> Carrera { get; set; }

        public DbSet<lasecExamen.Models.Log_Carrera> Log_Carrera { get; set; }
    }
