﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace lasecExamen.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Animal",
                columns: table => new
                {
                    id_animal = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    nombre_animal = table.Column<string>(maxLength: 50, nullable: false),
                    velicidad_animal = table.Column<int>(nullable: false),
                    descanso_animal = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Animal", x => x.id_animal);
                });

            migrationBuilder.CreateTable(
                name: "Carrera",
                columns: table => new
                {
                    id_carrera = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    tamano_carrera = table.Column<int>(nullable: false),
                    descanso_carrera = table.Column<int>(nullable: false),
                    tipo_carrera = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carrera", x => x.id_carrera);
                });

            migrationBuilder.CreateTable(
                name: "Log_Carrera",
                columns: table => new
                {
                    id_log_carrera = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    nombre_Animal = table.Column<string>(nullable: true),
                    nombre_CarreraId = table.Column<string>(nullable: true),
                    fecha_carrera_ganada = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Log_Carrera", x => x.id_log_carrera);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Animal");

            migrationBuilder.DropTable(
                name: "Carrera");

            migrationBuilder.DropTable(
                name: "Log_Carrera");
        }
    }
}
