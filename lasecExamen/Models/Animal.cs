﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using lasecExamen.Models;
using System.Collections.Generic;

namespace lasecExamen.Models{
    public class Animal{
        [Key]
		public int id_animal { get; set; }
		[Required (ErrorMessage ="Campo Requerido")]
		[StringLength(50,MinimumLength = 3,ErrorMessage ="El nombre debe tener de 3 a 50 caracteres")]
		public string nombre_animal { get; set; }
		[Required(ErrorMessage = "Campo Requerido")]
		public int velicidad_animal { get; set; }
		[Required(ErrorMessage = "Campo Requerido")]
		public int descanso_animal { get; set; }
    }
}
