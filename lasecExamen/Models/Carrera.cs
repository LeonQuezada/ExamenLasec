﻿using System;
using System.ComponentModel.DataAnnotations;

namespace lasecExamen.Models{
    public class Carrera{
		[Key]
		public int id_carrera { get; set; }
		[Required(ErrorMessage = "Campo Requerido")]
		public int tamano_carrera { get; set; }
		[Required(ErrorMessage = "Campo Requerido")]
		public int descanso_carrera { get; set; }
		[Required(ErrorMessage = "Campo Requerido")]
		[StringLength(50, MinimumLength = 3, ErrorMessage = "El nombre debe tener de 3 a 50 caracteres")]
		public string tipo_carrera { get; set; }
       
    }
}
