﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using lasecExamen.Models;
namespace lasecExamen.Models{
    public class Log_Carrera{      

        [Key]
		public int id_log_carrera { get; set; }
   
		public string nombre_Animal { get; set; }

		public string nombre_CarreraId { get; set; }

		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime fecha_carrera_ganada { get; set; }
    }
}
