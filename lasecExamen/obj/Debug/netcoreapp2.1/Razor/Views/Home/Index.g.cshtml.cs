#pragma checksum "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0d053564bcbad0902ed64df75123bf2e4c21cdde"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/_ViewImports.cshtml"
using lasecExamen;

#line default
#line hidden
#line 2 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/_ViewImports.cshtml"
using lasecExamen.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0d053564bcbad0902ed64df75123bf2e4c21cdde", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a7c3d942bbb6d7097d0b7aef2ae35162902176d2", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<lasecExamen.Controllers.ItemViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/homeIndex.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("/Log_Carrera/Create"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/Concurrent.Thread.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/homeIndex.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(46, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
            DefineSection("Styles", async() => {
                BeginContext(110, 104, true);
                WriteLiteral("\r\n    <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css\">\r\n\r\n    ");
                EndContext();
                BeginContext(214, 50, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "a15b45a30dde419b94036356c3c3f7a8", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(264, 5, true);
                WriteLiteral("\r\n \r\n");
                EndContext();
            }
            );
            BeginContext(272, 102, true);
            WriteLiteral("\r\n<h1>Fabula de la tortuga y la liebre</h1>\r\n\r\n\r\n    <select id=\"tipo_carrera\" class=\"form-control\">\r\n");
            EndContext();
#line 17 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
    
         int counter1 = 1;
        foreach (var item in Model.Carreras) {
            
            


#line default
#line hidden
            BeginContext(486, 12, true);
            WriteLiteral("            ");
            EndContext();
            BeginContext(498, 115, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1a524d01a56f48728380fb17273b62e6", async() => {
                BeginContext(524, 18, true);
                WriteLiteral("\r\n                ");
                EndContext();
                BeginContext(543, 47, false);
#line 24 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.tipo_carrera));

#line default
#line hidden
                EndContext();
                BeginContext(590, 14, true);
                WriteLiteral("\r\n            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            BeginWriteTagHelperAttribute();
#line 23 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
               WriteLiteral(counter1);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(613, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 26 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
            counter1 = counter1 + 1;  

        }


#line default
#line hidden
            BeginContext(673, 17, true);
            WriteLiteral("    </select>\r\n\r\n");
            EndContext();
#line 33 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
  
    int counter = 1;
    foreach (var item in Model.Animales) {

#line default
#line hidden
            BeginContext(760, 24, true);
            WriteLiteral("                    <div");
            EndContext();
            BeginWriteAttribute("id", " id=\"", 784, "\"", 804, 2);
            WriteAttributeValue("", 789, "imagen_", 789, 7, true);
#line 36 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
WriteAttributeValue("", 796, counter, 796, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(805, 78, true);
            WriteLiteral(">\r\n                     \r\n                    </div>\r\n                    <div");
            EndContext();
            BeginWriteAttribute("id", " id=\"", 883, "\"", 907, 2);
            WriteAttributeValue("", 888, "myProgress_", 888, 11, true);
#line 39 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
WriteAttributeValue("", 899, counter, 899, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(908, 31, true);
            WriteLiteral(">\r\n                        <div");
            EndContext();
            BeginWriteAttribute("id", " id=\"", 939, "\"", 958, 2);
            WriteAttributeValue("", 944, "myBar_", 944, 6, true);
#line 40 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
WriteAttributeValue("", 950, counter, 950, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(959, 106, true);
            WriteLiteral("></div>\r\n                    </div>\r\n                    <input id=\"id_animal\" type=\"hidden\" name=\"nombre\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1065, "\"", 1118, 1);
#line 42 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
WriteAttributeValue("", 1073, Html.DisplayFor(modelItem => item.id_animal), 1073, 45, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1119, 73, true);
            WriteLiteral("><br>\r\n                    <input id=\"nombre\" type=\"hidden\" name=\"nombre\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1192, "\"", 1249, 1);
#line 43 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
WriteAttributeValue("", 1200, Html.DisplayFor(modelItem => item.nombre_animal), 1200, 49, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1250, 79, true);
            WriteLiteral("><br>\r\n                    <input id=\"velocidad\" type=\"hidden\" name=\"velocidad\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1329, "\"", 1389, 1);
#line 44 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
WriteAttributeValue("", 1337, Html.DisplayFor(modelItem => item.velicidad_animal), 1337, 52, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1390, 77, true);
            WriteLiteral("><br>\r\n                    <input id=\"descanso\" type=\"hidden\" name=\"descanso\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1467, "\"", 1526, 1);
#line 45 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
WriteAttributeValue("", 1475, Html.DisplayFor(modelItem => item.descanso_animal), 1475, 51, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1527, 77, true);
            WriteLiteral("><br>\r\n                    <input id=\"contador\" type=\"hidden\" name=\"contador\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 1604, "\"", 1620, 1);
#line 46 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
WriteAttributeValue("", 1612, counter, 1612, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1621, 33, true);
            WriteLiteral("><br>\r\n                    <br>\r\n");
            EndContext();
#line 48 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
        counter = counter + 1;           
    }

#line default
#line hidden
            BeginContext(1707, 102, true);
            WriteLiteral("<button class=\"btn btn-primary\" onclick=\"hiloAnimales()\">Iniciar Carrera</button> \r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
            EndContext();
            BeginContext(1809, 1060, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8dc6cb1116ac4a7392089b301be2cbe9", async() => {
                BeginContext(1858, 1004, true);
                WriteLiteral(@"
            
            <div class="""">
                <input   style=""visibility:hidden"" class="""" type=""text"" id=""nombre_Animal"" name=""nombre_Animal"" value="""">
                <span class=""text-danger field-validation-valid"" data-valmsg-for=""nombre_Animal"" data-valmsg-replace=""true""></span>
            </div>
            <div class="""">
                <input style=""visibility:hidden""  class="""" type=""text"" id=""nombre_CarreraId"" name=""nombre_CarreraId"" value="""">
                <span class=""text-danger field-validation-valid"" data-valmsg-for=""nombre_CarreraId"" data-valmsg-replace=""true""></span>
            </div>
            <div class=""form-group"">
                <input type=""submit"" value=""Guardar Carrera"" class=""btn btn-default"">
            </div>
        <input name=""__RequestVerificationToken"" type=""hidden"" value=""CfDJ8KCZNnjwZNFHixtmow7KI0hf1hfeO0X-_2KvEotKfp44Oi0SWFGtdwOsdYt5qy7l6KNnr5HUWp3ny4EVQBv9JL_cKDVuJoB_eqvKbJgWQOaX_7CXbXWDmmIH-cH06ByC8Ku4NKhsx2NEGa02sZir22c"">");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2869, 8, true);
            WriteLiteral("\r\n\r\n\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(2895, 142, true);
                WriteLiteral("\r\n    <script src=\"//code.jquery.com/jquery-1.12.4.js\"></script>\r\n    <script src=\"//code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>\r\n\r\n    ");
                EndContext();
                BeginContext(3037, 49, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7e715a6f176e46389285b3ed51ed05a9", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3086, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(3092, 41, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3d8c87b6c7134a909830c81efa3db95a", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3133, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 84 "/home/adazeuqnoel/Projects/lasecExamen/lasecExamen/Views/Home/Index.cshtml"
      await Html.RenderPartialAsync("_ValidationScriptsPartial");

#line default
#line hidden
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<lasecExamen.Controllers.ItemViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
